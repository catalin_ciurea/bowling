#!/usr/bin/perl 
#===============================================================================
#
#         FILE: bowling.pl
#
#        USAGE: ./bowling.pl <scores_file>
#               perl bowling.pl <scores_file>
#
#  DESCRIPTION: this program is an implementation of the bowling scoring game
#               provided the nr of pins kicked by each throw we compute the score
#       AUTHOR: catalin
#      VERSION: 1.0
#===============================================================================
use strict;
use warnings;

sub bowling {
    # arrayref containing all the pins knowcked down by each hit
    # [ 3 , 4, 5, 6, 7, ...]
    my @pin_list = @{ shift() };

    # This is a var containing the previous score and it is used to print out the score
    # for the current frame using the score for the previous frame
    my $previous_score = shift;

    # grab the first hit from the list
    my $first = shift @pin_list;

    # we reached the end of the game
    return 0 unless defined $first;

    # this contains the current score which is about to be printed
    my $current_score = 0;

    # strike just recurse into the function
    if ($first == 10) {
        # compute current score and print
        $current_score = defined $previous_score ? $previous_score + 10 + $pin_list[0] + $pin_list[1]
                                                 : 10 + $pin_list[0] + $pin_list[1]
                                                 ;
        print $current_score, ' ';

        # special case in the last frame. Stop recursing
        if (@pin_list == 2) {
            return 10 + $pin_list[0] + $pin_list[1];
        }
        return 10 + $pin_list[0] + $pin_list[1] + bowling(\@pin_list, $current_score);
    }

    # shift a second element
    my $second = shift @pin_list;

    # we are on the last frame and a strike or a spare was hit 
    return 0 unless defined $second;

    # spare
    if ($first + $second == 10) {
        # compute current score and print
        $current_score = defined $previous_score ? $previous_score + 10 + $pin_list[0]
                                                 : 10 + $pin_list[0]
                                                 ;
        print $current_score, ' ';

        # special case in the last frame. Stop recursing
        if (@pin_list == 1) {
            return 10 + $pin_list[0];
        }
        return 10 + $pin_list[0] + bowling(\@pin_list, $current_score);
    }

    # regular case (no strike, no spare)

    # compute current score and print
    $current_score = defined $previous_score ? $previous_score + $first + $second 
                                             : $first + $second
                                             ;
    print $current_score, ' ';

    return $first + $second + bowling(\@pin_list, $current_score);
}


# Read the file in the cmd line and call bowling for each line

# we need at least one file to be provided on the cmd line
die 'No scores file provided!!' unless @ARGV;

# loop through the lines in the file(s) provided
while (<>) {
    s/^\s+|\s+$//;  # eliminate the spaces at the beginning and at the end of the line

    next if /^#/;   # skip comments
    next if /^\s*$/;   # skip empty lines

    my @next_game = split /\s+/; # grab the score line as a list of integers

    # min nr of strikes is 12 for a perfect game (only strikes)
    # max nr of strikes is 21 for a game with 2 hits on each frame (no strikes) + 
    #   a strike or a spare in the last frame which results in 3 hits
    local $" = q{ }; 
    print(qq{Invalid game score list: @next_game}, "\n"), next
        if @next_game < 12 || @next_game > 21;

    bowling(\@next_game); # call bowling function for each line
    print "\n\n"; # print a new line to make it look nicer
}

__END__
