# Bowling scoring implementation in perl

* This perl script prints the score for each frame
* It accepts a file containing lists of scores sepparated by space (NOTE: one line for each game)

___
## 1. Run Example (from a Unix shell)

    %> ./bowling.pl scores_file
    %> perl bowling.pl scores_file

___
## 2. Scores file    
   
* The scores file contains the scores for multiple games one list of scores per line corresponding to a game.   
* The lines starting with a ```#``` and blank lines are ignored.
* Spares and Strikes are not specially marked in the scores file. A strike is simply marked as a nr 10 in the scores list and a spare means that the sum of the 2 hits is 10.
___
    # scores for game 1
    1 4  4 5  6 4  5 5  10  0 1  7 3  6 4  10  2 8 6
    # scores for game 2
    10 10 10 10 10 10 10 10 10 10 10 10


## 3. Tests present in ```scores_file``` file in the repo.

    # game 1
    1 4  4 5  6 4  5 5  10  0 1  7 3  6 4  10  2 8 6
    
    # output game 1
    5 14 29 49 60 61 77 97 117 133
    
___

    # game 2
    10  10  10  10  10  10  10  10  10  10  10  10
    
    # output game 2
    30 60 90 120 150 180 210 240 270 300
    
___

    # game 3
    9 0  3 5  6 1  3 6  8 1  5 3  2 5  8 0  7 1  8 1
    
    # output game 3
    9 17 24 33 42 50 57 65 73 82
    
___

    # game 4
    9 0  3 7  6 1  3 7  8 1  5 5  0 10  8 0  7 3  8 10 8
    
    # output game 4
    9 25 32 50 59 69 87 95 113 131
    
___
    
    # game 5
    10  3 7  6 1  10  10  10  2 8  9 0  7 3  10 10 10
    
    # output game 5
    20 36 43 73 95 115 134 143 163 193
    
___

    # game 6
    10  7 3  9 0  10  0 8  8 2  0 6  10  10  10 8 1

    # output game 6
    20 39 48 66 74 84 90 120 148 167

___
[Catalin Ciurea](mailto:catalin@cpan.org)




